
require 'rspec'
require 'bigdecimal'
require_relative '../../lib/helpers/orderbook_helper'

class Sprout
  include OrderBookHelper
end

RSpec.describe OrderBookHelper do
  subject { Sprout.new }

  it { should respond_to :exchange_ratio }
  it { should respond_to :prepare_data   }
  it { should respond_to :sort_data      }
  it { should respond_to :select_assets  }

  describe '.exchange_ratio method' do
    let(:price) { BigDecimal('477.78'    ) }
    let(:units) { BigDecimal('0.83472113') }

    it 'gets the first asset\'s quantity for a second\'s asset unit' do
      expected = BigDecimal '572.382778904854'
      delta    = 0.000000001
      expect(subject.exchange_ratio price, units).to be_within(delta).of(expected)
    end
  end

  describe '.prepare_data method' do
    before do
      allow(BigDecimal).to receive(:new           ) { 'object' }
      allow(subject   ).to receive(:exchange_ratio) { 'ratio'  }
    end

    let(:asset_set) { [["value", "value"], ["value", "value"]]               }
    let(:expected ) { [{ price: 'object', units: 'object', ratio: 'ratio' }] }

    it 'formats an array of assets strings into hashes' do
      expect(subject.prepare_data(asset_set)[0]  ).to eq expected[0]
    end

    it 'formats all of the asset pairs' do
      expect(subject.prepare_data(asset_set).size).to eq asset_set.size
    end
  end

  describe '.sort_data method' do
    context 'with an array of prepared assets' do
      let(:assets) {
        [
          { ratio: BigDecimal('472.84615357046153') },
          { ratio: BigDecimal('293.70461538461535') },
          { ratio: BigDecimal('393.15357046153846') }
        ]
      }

      it 'sorts the assets by the ratio key' do
        expect(subject.sort_data(assets)[0]).to eql assets[1]
      end
    end
  end

  describe '.select_assets method' do
    let(:asset_group) do
      [
        {
          price: BigDecimal('477.10'    ),
          units: BigDecimal('0.00000002'),
          ratio: BigDecimal('238.55')
        }, {
          price: BigDecimal('477.27'    ),
          units: BigDecimal('1.62500000'),
          ratio: BigDecimal('293.70461538461535')
        }, {
          price: BigDecimal('477.78'    ),
          units: BigDecimal('0.83472113'),
          ratio: BigDecimal('572.382778904854')
        }, {
          price: BigDecimal('477.52'    ),
          units: BigDecimal('0.53100000'),
          ratio: BigDecimal('899.2843691148776')
        }, {
          price: BigDecimal('477.17'    ),
          units: BigDecimal('0.17500000'),
          ratio: BigDecimal('2726.6857142857148')
        }
      ]
    end

    context 'with a value that affords to get other assets' do
      let(:value ) { BigDecimal('1000.0')                              }
      let(:assets) { subject.select_assets(asset_group, value, :price) }

      specify ', each selected is affordable' do
        initial_value = value

        assets.each do |asset_pair|
          expect(initial_value).to be > asset_pair[:price]
        end
      end

      specify ', the group of selected assets is affordable' do
        ammount       = BigDecimal('0')
        initial_value = value

        assets.each { |asset_pair| ammount += asset_pair[:price] }

        expect(ammount).to be < initial_value
      end
    end
  end
end
