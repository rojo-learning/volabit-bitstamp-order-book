
require 'rspec'
require 'json'
require_relative '../lib/orderbook'

RSpec.shared_examples 'a prepared dataset' do
  describe 'asset pairs have a hash representation' do
    specify 'with a key for the 1st asset' do
      expect(asset_pair[:price]).to be
    end

    specify 'with a key for the 2nd asset' do
      expect(asset_pair[:units]).to be
    end

    specify 'with a key for the exchange ratio' do
      expect(asset_pair[:ratio]).to be
    end
  end

  describe 'each asset has an object representation' do
    let(:asset) { asset_pair[:price] }

    specify 'of the BigDecimal class' do
      expect(asset.class).to eql(BigDecimal)
    end
  end
end

RSpec.describe OrderBook do
  let(:order_book) do
    JSON.parse(File.open('responses/order_book.json', 'r') { |f| f.read })
  end

  subject { described_class.new order_book }

  it { should respond_to :bids       }
  it { should respond_to :asks       }
  it { should respond_to :timestamp  }
  it { should respond_to :bid_intend }
  it { should respond_to :ask_intend }

  context 'class is instantiated' do
    describe 'with «bids» correctly set' do
      let(:asset_pair) { subject.bids[rand(subject.bids.size)]               }
      let(:some_pairs) { [subject.bids[0], subject.bids[1], subject.bids[2]] }

      it_behaves_like 'a prepared dataset'

      specify 'asset pairs are sorted by ascending exchange ratio' do
        expect(some_pairs[0][:ratio]).to be <= some_pairs[1][:ratio]
        expect(some_pairs[1][:ratio]).to be <= some_pairs[2][:ratio]
      end
    end

    describe 'with «asks» correctly set' do
      let(:asset_pair) { subject.asks[rand(subject.asks.size)]               }
      let(:some_pairs) { [subject.asks[0], subject.asks[1], subject.asks[2]] }

      it_behaves_like 'a prepared dataset'

      specify 'asset pairs are sorted by descending exchange ratio' do
        expect(some_pairs[0][:ratio]).to be >= some_pairs[1][:ratio]
        expect(some_pairs[1][:ratio]).to be >= some_pairs[2][:ratio]
      end
    end

    specify 'with «timestamp» correctly set' do
      expect(subject.timestamp).to eql order_book['timestamp']
    end
  end

  describe '.bid_intend method' do
    # It's only an interface to .select_assets
  end

  describe '.ask_intend method' do
    # It's only an interface to .select_assets
  end
end
