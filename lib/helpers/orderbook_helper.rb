
# Internal: Private methods used by the Transaction class.
# This module must be included under the private section of the OrderBook class.
module OrderBookHelper

  # Internal: Calculates the exchange ratio of one asset for other one. (Like
  # dollars per bitcoin.)
  #
  # asset_one - Asset that represents what should be given. (Or the price paid.)
  # asset_two - Asset that represents what would be received. (Units of it.)
  #
  # Examples
  #
  #   dollars  = BigDecimal '100.0'
  #   bitcoins = BigDecimal '0.24586902'
  #   OrderBookHelper.exchange_ratio dollars, bitcoins
  #   # => 406.72061897021433
  #
  # Returns a Float that represents the units of the asset_one required to get
  # one unit of the asset_two.
  def exchange_ratio(asset_one, asset_two)
    asset_one / asset_two
  end

  # Internal: Builds a Array of Asset pairs contained in Hashes from its
  # representation as an Array of string pairs.
  #
  # asset_array - Array of arrays containing string asset pairs representing
  #               bids or asks: [['value', 'value']].
  #
  # Examples
  #
  #   bids = [['100.0', '0.24586902'], ...]
  #   OrderBookHelper.prepare_data bids
  #   # => [{ price: BigDecimal, units: BigDecimal, ratio: BigDecimal }, ...]
  #
  # Returns an Array that holds several Assets pairs, each pair tied in a Hash
  # which also include the exchange ratio between them.
  def prepare_data(asset_array)
    [].tap do |assets|
      asset_array.each do |price, units|
        price = BigDecimal.new price
        units = BigDecimal.new units
        ratio = exchange_ratio(price, units)

        assets << { price: price, units: units, ratio: ratio }
      end
    end
  end

  # Internal: Sorts an Array of hashed asset pairs by their exchange ratio,
  # ascending.
  #
  # asset_array - Array of hashes containing Asset pairs representing
  #               bids or asks: [{ asset: , asset: , ratio: }].
  #
  # Examples
  #
  #   bids = [{..., ratio: 3}, {..., ratio: 5}, {..., ratio: 1}]
  #   OrderBookHelper.sort_data bids
  #   # => [{..., ratio: 1}, {..., ratio: 3}, {..., ratio: 5}]
  #
  # Returns an sorted version Array of the received array, by ascending ratio.
  def sort_data(prepared_assets)
    prepared_assets.sort_by { |asset_pair| asset_pair[:ratio] }
  end

  # Internal: Selects a group of asset pairs whose total price (sum of their
  # 'price' key) is lower or equal that the value of a given exchange asset.
  #
  # asset_group    - Array of hashes containing Asset pairs representing
  #                  bids or asks: [{ asset: , asset: , ratio: }].
  # exchange_asset - Asset that would be exchanged for other Asset(s).
  # selector       - Symbol that indicates the key of the asset (in the asset
  #                  pair) that is compared with the exchange asset.
  #
  # Examples
  #
  #   exchange.decimal # => 100.0
  #   bids.each { |pair| pair[:price].to_f }
  #   # => 20.3 102.4 10.34 50.45 80.21
  #   OrderBookHelper.select_assets(bids, exchange, :price).each do |asset_pair|
  #     pair[:price]
  #   end
  #   # => 20.3 10.34 50.45
  #
  # Returns an Array the selected hashed Asset pairs, being the total sum of
  # their decimal value, in the exchange key, less or equal that the decimal
  # value of the exchange asset.
  def select_assets(asset_group, exchange_asset, selector)
    filtered_assets = asset_group.select do |asset_pair|
      asset_pair[selector] < exchange_asset
    end

    [].tap do |selected_assets|
      filtered_assets.each do |asset_pair|
        break unless asset_pair[selector] < exchange_asset

        selected_assets << asset_pair
        exchange_asset  -= asset_pair[selector]
      end
    end

  end
end
