
require_relative 'helpers/orderbook_helper'

# Internal: Class that represents the order book state at a give time, holding
# the data of the bids, the asks and the timestamp when they where retrieved.
#
# Examples
#
#  OrderBook.new { bids: ..., asks: ..., timestamp: "1410201805" }
#  # => #<OrderBook:id @bids=[...], @asks=[...], @timestamp="1410201805">
#
class OrderBook
  # Public: Returns an Array of bid Asset pairs contained within a Hash each.
  attr_reader :bids
  # Public: Returns an Array of ask Asset pairs contained within a Hash each.
  attr_reader :asks
  # Returns the Unix timestamp of the retrieved data.
  attr_reader :timestamp

  # Public: Initialize an OrderBook.
  #
  # orderbook_data - Hash that contains an Array of string pairs that represent
  #                  asset values in bids, other Array of the same kind for asks
  #                  and the Unix timestamp of the retrieved data.
  def initialize(orderbook_data)
    @timestamp = orderbook_data['timestamp']
    # List first the bids with a lower number of dollars per bitcoin
    @bids = sort_data(prepare_data orderbook_data['bids'])
    # List first the asks with a higer number of dollars per bitcoin
    @asks = sort_data(prepare_data orderbook_data['asks']).reverse
  end

  # Public: Selects the bids that would be processed (exchanged) given a string
  # representing a decimal that is the available resource to exchange.
  #
  # Examples
  #
  #    instance.bind_intend '100.0'
  #    # => [{'asset pair'}, {'asset pair'}, {'asset pair'}, ...]
  #
  # Returns an Array of the selected bids whose total value is less than the
  # value available for exchange.
  def bid_intend(value_string)
    select_assets(@bids, BigDecimal(value_string), :price)
  end

  # Public: Selects the asks that would be processed (exchanged) given a string
  # representing a decimal that is the available resource to exchange.
  #
  # Examples
  #
  #    instance.ask_intend '8.38475893'
  #    # => [{'asset pair'}, {'asset pair'}, {'asset pair'}, ...]
  #
  # Returns an Array of the selected asks whose total value is less than the
  # value available for exchange.
  def ask_intend(value_string)
    select_assets(@asks, BigDecimal(value_string), :units)
  end

  private
  include OrderBookHelper
end
