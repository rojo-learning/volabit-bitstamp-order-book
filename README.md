# Coding for Fun! Bitstamp Order Book
A little program that is able to consume the [Bitstamp public API](https://www.bitstamp.net/api/) for the _order book_ info. It can emulate _bids_ and _asks_ transactions as intends, receiving an amount of dollars or bitcoins to sell, returning the quantity of the value that would be expended, the quantity of the value that would obtained, and the marked deep for the transaction.

## WARNING!

This code uses the v1 of the Bitstamp's API that was avilable during 2014. Running the code examples with __live data__ may require to update the API consumption and using them _as is_ will likely not work.

## Install and Use

*Prerequisites*

 - Ruby 2.1.5. (It should work with 1.9.3.)
 - `bundle` gem to run the test suite.

*Start the interactive sample program*

`ruby example/init.rb`

## Running Tests

`bundle install`

`bundle exec rspec specs/`


### Note
By default, the program uses the provided example data under `responses/`. To use live data from Bitstamp, the following lines should be changed inside `init.rb`.

```
# JSON.parse(Net::HTTP.get_response(URI.parse url).body)
JSON.parse(File.open('responses/order_book.json', 'r') { |f| f.read })
```
to
```
JSON.parse(Net::HTTP.get_response(URI.parse url).body)
# JSON.parse(File.open('responses/order_book.json', 'r') { |f| f.read })
```

## Running Tests
```
bundle install

bundle exec rspec specs/
```

---
The code in this repository should be considered **public domain**.