
#!/usr/bin/env ruby

require 'bigdecimal'
require 'json'
require 'net/http'
require_relative '../lib/orderbook'

class Main
  def initialize
    @data = OrderBook.new bitstamp_order_book
    start
  end

  def bitstamp_order_book
    # Uncomment/Comment the next lines to switch from test data to live call
    # base = 'https://www.bitstamp.net/api/order_book/'
    # url  = base + "?group=1"
    # JSON.parse(Net::HTTP.get_response(URI.parse url).body)
    # Uncomment/Comment the next line to switch from live call to test data
    JSON.parse(File.open('responses/order_book.json', 'r') { |f| f.read })
  end

  def buy
    print 'How much dollars you plan to spend? (decimal format) '
    bids   = @data.bid_intend gets.chomp
    result = summarize bids

    puts "Weighted Average: #{result[:ratio].to_f} dollars/bitcoin"
    puts "Dollars that would be spent: #{result[:dollars].to_f};"
    puts "Bitcoins that would be obtained: #{result[:bitcoins].to_f}"
  end

  def sell
    print 'How much bitcoins you plan to sell? (decimal format) '
    asks   = @data.ask_intend gets.chomp
    result = summarize asks

    puts "Weighted Average: #{result[:ratio].to_f} dollars/bitcoin"
    puts "Bitcoins that would be spent: #{result[:bitcoins].to_f};"
    puts "Dollars that would be obtained: #{result[:dollars].to_f}"
  end

  def summarize(asset_group)
    dollars  = BigDecimal('0')
    bitcoins = BigDecimal('0')
    ratio    = BigDecimal('0')

    asset_group.each do |asset_pair|
      dollars  += asset_pair[:price]
      bitcoins += asset_pair[:units]
      ratio    += asset_pair[:ratio]
    end

    { dollars: dollars, bitcoins: bitcoins, ratio: ratio / asset_group.size }
  end

  def start
    puts 'Bitstamp - Order Book data is available'

    while true
      puts  'What would you like to do?'
      puts  'a) Buy bitcoins. b) Sell bitcoins, q) Exit.'
      print 'Option: '

      case gets.chomp.downcase
      when 'a' then buy
      when 'b' then sell
      when 'q' then  break
      else
        puts 'Chosen option is not available.'
      end
    end
  end
end

Main.new
